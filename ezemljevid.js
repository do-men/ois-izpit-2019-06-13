if (!process.env.PORT)
  process.env.PORT = 8080;


// Priprava strežnika
var express = require('express');
var streznik = express();
streznik.set('view engine', 'ejs');
streznik.use(express.static('public'));


var vremenske_slike = {
  "oblačno": "iconfinder_05_cloud_smile_cloudy_emoticon_weather_smiley_3375695.png",
  "deževno": "iconfinder_06_rain_cloud_cry_emoticon_weather_smiley_3375694.png",
  "sončno": "iconfinder_07_sun_smile_happy_emoticon_weather_smiley_3375693.png",
  "sneženo": "iconfinder_10_snow_coud_emoticon_weather_smiley_3375690.png",
  "pretežno oblačno": "iconfinder_09_cloudy_sun_happy_emoticon_weather_smiley_3375691.png"
};


var seznam_krajev = [];


// Prikaz zemljevida vremena
streznik.get("/", function (zahteva, odgovor) {
  odgovor.redirect('/zemljevid-vremena');
});


// Storitev, ki vrne vremenske podatke za podano poštno številko
streznik.get("/vreme/:postnaStevilka", function(zahteva, odgovor) {
  var postnaStevilka = parseInt(zahteva.params.postnaStevilka, 10);
  
  var postnaFirstDigit = Math.floor(postnaStevilka/1000);
  
  if (postnaStevilka == undefined || isNaN(postnaStevilka)) {
    odgovor.send(404, "Manjka ustrezna poštna številka!");
  } else {
    var postnaFirstDigit = Math.floor(postnaStevilka/1000);
    if (postnaFirstDigit == 1 || postnaFirstDigit == 4){
      var rezultat = {
        opis: "deževno",
        ikona: "iconfinder_06_rain_cloud_cry_emoticon_weather_smiley_3375694.png",
        temperatura: Math.floor(Math.random() * (20 - 5 + 1) + 5)
      };
    } else if (postnaFirstDigit == 2 || postnaFirstDigit == 3 || postnaFirstDigit == 9){
      var rezultat = {
        opis: "sončno",
        ikona: "iconfinder_07_sun_smile_happy_emoticon_weather_smiley_3375693.png",
        temperatura: Math.floor(Math.random() * (35 - 20 + 1) + 20)
      };
    } else if (postnaFirstDigit == 5 || postnaFirstDigit == 6){
      var rezultat = {
        opis: "pretežno oblačno",
        ikona: "iconfinder_09_cloudy_sun_happy_emoticon_weather_smiley_3375691.png",
        temperatura: Math.floor(Math.random() * (25 - 15 +1) + 15)
      };
    } else if (postnaFirstDigit == 8){
      var rezultat = {
        opis: "sneženo",
        ikona: "iconfinder_10_snow_coud_emoticon_weather_smiley_3375690.png",
        temperatura: Math.floor(Math.random() * (-15 + 5 + 1) - 5)
      };
    } else {
      var rezultat = {
        opis: "oblačno",
        ikona: "iconfinder_05_cloud_smile_cloudy_emoticon_weather_smiley_3375695.png",
        temperatura: Math.floor(Math.random() * (30 - 5 + 1) + 5)
      };
    }
  
    odgovor.send(rezultat);
  }
});


// Prikaz zgodovine
streznik.get("/zgodovina", function (zahteva, odgovor) {
  odgovor.send(seznam_krajev);
});


// Prikaz strani z zemljevidom
streznik.get("/zemljevid-vremena", function (zahteva, odgovor) {
  odgovor.render('zemljevid-vremena');
});


streznik.get("/zabelezi-zadnji-kraj/:json", function(zahteva, odgovor) {
  var rezultat = zahteva.params.json;
  seznam_krajev.push(JSON.parse(rezultat));
  odgovor.send({steviloKrajev: seznam_krajev.length});
});


streznik.listen(process.env.PORT, function () {
  console.log("Strežnik je pognan!");
});
