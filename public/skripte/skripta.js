/* global L, $ */

// seznam z markerji na mapi
var markerji = [];

var mapa;

const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;

/**
 * Ko se stran naloži, se izvedejo ukazi spodnje funkcije
 */
window.addEventListener('load', function () {
  
  //$("#bliznjiKraji").html("<h3>Bližji kraji</h3>");

  // Osnovne lastnosti mape
  var mapOptions = {
    center: [FRI_LAT, FRI_LNG],
    zoom: 9,
    maxZoom: 12
  };

  // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);

  // Akcija ob kliku na gumb za prikaz zgodovine
  $("#prikaziZgodovino").click(function() {
    window.location.href = "/zgodovina";
  });
  
  
  var getJSON = function(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
      var status = xhr.status;
      if (status === 200) {
        callback(null, xhr.response);
      } else {
        callback(status, xhr.response);
      }
    };
    xhr.send();
};


  function obKlikuNaMapo(e) {
    // Izbriši prejšnje oznake
    for (var i = 0; i < markerji.length; i++) {
      mapa.removeLayer(markerji[i]);
    }
    
    //document.getElementById("bliznjiKraji").innerHTML = counter;

    var koordinate = e.latlng;
    console.log(koordinate);
    //console.log(koordinate['lat']);
    //console.log(koordinate['lng']);
    
    var tmpLat = koordinate['lat'];
    var tmpLng = koordinate['lng'];
    
    console.log(tmpLat);
    console.log(tmpLng);
    
    var tmpUrl = "https://api.lavbic.net/kraji/lokacija?lat=" + tmpLat + "&lng=" + tmpLng;
    console.log(tmpUrl);
    
    getJSON(tmpUrl,
function(err, data) {
  if (err !== null) {
    alert('Something went wrong: ' + err);
  } else {
    var text = "<p><h3>Bližnji kraji</h3>"
    console.log(data);
    
      var okolica = "<p><h3> Vreme v okolici kraja<br>";
      okolica = okolica+data[0]['kraj']+"</h3>";
      
      
      okolica = okolica+"</p>";
    
      $("#vremeOkolica").html(okolica);
    
      for (var i = 0; i<5; i++){
        console.log(data[i]['postnaStevilka']);
        var tmpText = "<b>" + data[i]['postnaStevilka'] + " </b>" +''+data[i]['kraj'] + "<br>";
        text = text+tmpText;
      }

      //$("#bliznjiKraji").html("<h3>HELLO WORLD</h3>");
      text = text+"</p>";
      $("#bliznjiKraji").html(text);
      console.log(text);
  }
});
    
    

    // Dodaj trenutno oznako
    dodajMarker(koordinate.lat, koordinate.lng,
      "<b>Izbrana lokacija</b><br>(" + koordinate.lat + ", " + koordinate.lng + ")");

    // Prikaži vremenske podatke najbližjega kraja
    $("#vremeOkolica").html("<p>Vreme v okolici kraja<br>...<br></p>");

    // Shrani podatke najbližjega kraja
    $.get('/zabelezi-zadnji-kraj/' + JSON.stringify({
      postnaStevilka: 1000,
      kraj: "Ljubljana",
      vreme: "neznano",
      temperatura: 10
    }), function(t) {
      $("#prikaziZgodovino").html("Prikaži zgodovino ... krajev");
    });

    // Prikaži seznam 5 najbližjih krajev
    $("#bliznjiKraji").html("<p>Bližnji kraji<br>...<br>...<br>...<br>...<br>...</p>");
  }

  mapa.on('click', obKlikuNaMapo);

  // Na začetku postavi lokacijo na FRI
  var FRIpoint = new L.LatLng(FRI_LAT, FRI_LNG);
  mapa.fireEvent('click', {
    latlng: FRIpoint,
    layerPoint: mapa.latLngToLayerPoint(FRIpoint),
    containerPoint: mapa.latLngToContainerPoint(FRIpoint)
  });
});


/**
 * Dodaj izbrano oznako na zemljevid na določenih GPS koordinatah
 *
 * @param lat zemljepisna širina
 * @param lng zemljepisna dolžina
 * @param opis sporočilo, ki se prikaže v oblačku
 */
function dodajMarker(lat, lng, opis) {
  var ikona = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
      'marker-icon-2x-blue.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
      'marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

  var marker = L.marker([lat, lng], {icon: ikona});
  marker.bindPopup("<div>" + opis + "</div>").openPopup();
  marker.addTo(mapa);
  markerji.push(marker);
}
